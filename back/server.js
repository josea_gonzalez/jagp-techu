const config  = require('./config');
// Cargamos la libreria express



const express = require('express');
//Libreria cors para desarrollo
const cors = require('cors');

//const logger          = require('morgan')


const jwt = require('express-jwt');
const dotenv = require('dotenv').config();
const bodyParser =require('body-parser');

const app = express(); //Cargamos las funciones del framework
const port = process.env.PORT || 3001;

// Parseo de JSON

app.use(bodyParser.json())
app.use(cors())



// Parseo de JSON
app.listen(port);
//dotenv.load();







console.log("App running!" + port);



const userController = require('./controllers/UserController');
const accountController = require('./controllers/AccountController');
const sessionController = require('./controllers/SessionController');





var jwtCheck = jwt({
  secret: config.secret,
  audience: config.audience,
  issuer: config.issuer
  });


  function requireScope(scope) {


    return function (req, res, next) {
      console.log(req.user.scope)
      var has_scopes = req.user.scope === scope;
      if (!has_scopes) {
          res.sendStatus(401);
          return;
      }
      next();
    };
  }



// Protegemos las llamadas
app.use('/jagpapi/v1/users/:id/accounts/', jwtCheck, requireScope('full_access'));
app.use('/jagpapi/v1/events/', jwtCheck, requireScope('full_access'));

// Recupera las cuentas de un usuario



// Obtiene los datos de un usuario
app.get('/jagpapi/v1/users/:email/', userController.getUser)

// Obtiene las cuentas de un usuario
app.get("/jagpapi/v1/users/:id/accounts/", userController.getAccountById);
  // Recupera las cuentas de un usuario


// Crea un nuevo usuario
app.post('/jagpapi/v1/users/create/', userController.createUser);

app.post('/jagpapi/v1/users/:email/update/account', userController.createAccount);

// Actualiza los datos de un usuario
app.post('/jagpapi/v1/users/:email/update/', userController.updateUser);

// Crear la sesión
app.post("/jagpapi/v1/sessions/create/", sessionController.createSession);
// Borra la sesión

app.post("/jagpapi/v1/sessions/delete/", sessionController.deleteSession);





app.get("/jagpapi/v1/events/:iban/accounts/", accountController.getEvents);
app.get("/jagpapi/v1/balance/:iban/accounts/", accountController.getBalance);

  // Crea un nuevo evento
app.post("/jagpapi/v1/events/:iban/accounts/", accountController.createEvent);
