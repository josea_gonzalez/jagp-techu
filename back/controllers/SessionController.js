const dotenv = require('dotenv').config();
const config  = require('../config');
//const io = require('../io');
const requestJson = require('request-json')
//const process.env.BASEMLABURL = "https://api.mlab.com/api/1/databases/apitechu/collections/";
//const process.env.MLABAPIKEY = "apiKey=G16LdGcBQ88ZyjId7l-Kgsa3ENxsOJIG"
const crypt = require('../crypt')
const jwt = require('jsonwebtoken')
//const config  = require('./config')
const _ = require('lodash')


function createIdToken(user) {
  console.log(jwt.sign(_.omit(user, 'password'), config.secret, { expiresIn: 60*60*5 }))
  return jwt.sign(_.omit(user, 'password'), config.secret, { expiresIn: 60*60*5 });
}

function createAccessToken() {
  console.log('createAccessToken')
  return jwt.sign({
    iss: config.issuer,
    aud: config.audience,
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    scope: 'full_access',
    sub: "lalaland|gonto",
    jti: genJti(), // unique identifier for the token
    alg: 'HS256'
  }, config.secret);
}

function genJti() {
  console.log('genJti')
  let jti = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 16; i++) {
      jti += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return jti;
}

function createSession(req, res) {
 console.log("POST createSession");
 console.log("BODY: ",req.body)
 var email = req.body.email;
 var password = req.body.password;

 var query = 'q={"email": "' + email + '"}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(process.env.BASEMLABURL);
 httpClient.get("user?" + query + "&" + process.env.MLABAPIKEY,
   function(err, resMLab, body) {
     console.log("longitud del body: ", body.length)
     console.log("contenido: ", body)
     if (body.length == 0) {
       var response = {
         "mensaje" : "Usuaro no existe"
       }
       res.status(404).send("Usuario incorrecto");
       //res.send(response);
     } else {
       var isPasswordcorrect =
         crypt.checkPassword(password, body[0].password);
         console.log("Password correct is " + isPasswordcorrect);
         if (isPasswordcorrect){
           console.log("Got a user with that email and password, logging in");
           query = 'q={"email" : "' + body[0].email +'"}';
           console.log("Query for put is " + query);
           var putBody = '{"$set":{"logged":true}}';
           httpClient.put("user?" + query + "&" + process.env.MLABAPIKEY, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT done");

              var response = {
                "email" : body[0].email,
                "first_name": body[0].first_name,
                "last_name": body[0].last_name
              }
              console.log("Response: ", response)

              res.status(201).send({
                id_token: createIdToken(email),
                access_token: createAccessToken(),
                user: response,
              });
            }
          )
        }
        else{
          console.log("Contraseña incorrecta")

          res.status(401).send(
            "Invalid Login"
          );
        }

     }
   }
 );
}


function deleteSession(req,res){
  console.log("deleteSession ESTE ES EL BUENO!")
  //console.log(req)
  console.log("req.body", req.body)
  var email = req.body.user.email

  var query = 'q={"email": "' + email + '"}';
  console.log("query es " + query);

  httpClient = requestJson.createClient(process.env.BASEMLABURL);
  httpClient.get("user?" + query + "&" + process.env.MLABAPIKEY,
   function(err, resMLab, body) {
     console.log("MLAB BODY: ", body)
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that email, logging out");
       query = 'q={"email" : "' + email +'"}';
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + process.env.MLABAPIKEY, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "email" : email
           }
           res.send(response);
         }
       )
     }
   }
 );
}


module.exports.createSession = createSession;
module.exports.deleteSession = deleteSession;
//module.exports.loginV2 = loginV2;
