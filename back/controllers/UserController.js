
const config  = require('../config');
const MongoClient = require('mongodb').MongoClient;


const requestJson = require('request-json')
const crypt = require('../crypt.js')

const jwt = require('jsonwebtoken')
const _ = require('lodash')



function createIdToken(user) {
  console.log('createIdToken')
  return jwt.sign(_.omit(user, 'password'), config.secret, { expiresIn: 60*60*5 });
}

function createAccessToken() {
  console.log('createAccessToken')
  return jwt.sign({
    iss: config.issuer,
    aud: config.audience,
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    scope: 'full_access',
    sub: "lalaland|gonto",
    jti: genJti(), // unique identifier for the token
    alg: 'HS256'
  }, config.secret);
}

function genJti() {
  console.log('genJti')
  let jti = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 16; i++) {
      jti += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return jti;
}



function createUser(req,res){
  console.log("createUser")

  var httpClient = requestJson.createClient(process.env.MOCKAROOURL);

  httpClient.get("9a9512a0?count=1" + "&" + process.env.MOCKAROOAPIKEY,
   function(errMock, resMock, bodyMock){
     if (errMock){
       console.log("error")
       var response = {
         "msg" : "Error conexión Mockaroo"
       }
       resMock.status(500).send(response)
     } else {
       console.log("no error")
       if (bodyMock.length > 0){
         var iban = bodyMock;
         console.log("Respuesta de mockaroo: ",iban)

         var newUser = {
           // "id" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "accounts": iban,
           "logged" :  true,
           "password" : crypt.hash(req.body.password)
         }
         var httpClient = requestJson.createClient(process.env.BASEMLABURL);
         httpClient.post("user?"  + process.env.MLABAPIKEY, newUser,
          function(err, resMLab, body){

            if (err){
              console.log('ERROR dando de alta el usuario')
              console.log(err)
              var response = {
                "msg" : "Error conexión"
              }
              res.status(500).send(response)
            }
            else{
              console.log('err: ', err)
              console.log('resMLab: ', resMLab)
              console.log("Usuario creado con éxito")
              //res.send({"msg": "Usuario creado con éxito"})
              res.status(201).send({
                id_token: createIdToken(req.body.email),
                access_token: createAccessToken(),
                user: body,
              });
            } // Fin Else
          }
         )
       } else {
         var response = {
           "msg" : "Error recuperando iban de mockaroo"
         }
         console.log(response)
         res.status(404).send(response)
       }
     }
   }
 );
}

function updateUser(req,res){
  console.log("updateUser")
  console.log(req.params)
  var email = req.params.email
  var first_name = req.body.first_name
  var last_name = req.body.last_name
  //var password = req.body.password
  console.log(email)
  console.log(first_name)
  console.log(last_name)
  //console.log(password)
  MongoClient.connect(process.env.URLMONGO, function(err, client) {
    if (err)
    {
      var response = {
        "msg" : "Error conectando base de datos"
      }
      res.status(500).send(response)

    }
    else
    {
      console.log("Connected successfully to server");
      const db = client.db(process.env.DBNAME);
      const collection = db.collection('user')

      collection.updateOne(
        {"email":email},
        {$set:{
          "first_name": first_name,
          "last_name": last_name,
          "email" : email
        }}
      ,function(err, docs) {
          if (err){
            console.log('Error')
            var response = {
            "msg" : "Error actualizando datos del usuario"
            }
          res.status(400).send(response)
          }
        else{
          let userUpdate = {
            "first_name": first_name,
            "last_name": last_name,
            "email" : email
          }


          res.status(201).send({
            id_token: createIdToken(req.body.email),
            access_token: createAccessToken(),
            user: userUpdate})
        }
      });

      client.close();
    }

  });
}



function createAccount(req,res){
  var email = req.params.email
  var httpClient = requestJson.createClient(process.env.MOCKAROOURL);

  httpClient.get("9a9512a0?count=1" + "&" + process.env.MOCKAROOAPIKEY,
   function(errMock, resMock, bodyMock){
     if (errMock){
       console.log("error")
       var response = {
         "msg" : "Error obteniendo iban"
       }
       resMock.status(500).send(response)
     }
      else {
       console.log("no error")
       if (bodyMock.length > 0){
         var iban = bodyMock[0].iban;
         console.log("Respuesta de mockaroo: ",iban)
         MongoClient.connect(process.env.URLMONGO, function(err, client) {
           if (err)
           {
             var response = {
               "msg" : "Error conectando base de datos"
             }
             res.status(500).send(response)

           }
           else
           {
             console.log("Connected successfully to server");
             const db = client.db(process.env.DBNAME);
             const collection = db.collection('user')

             collection.updateOne(
               {"email":email},
               {$push:{accounts:{iban}}}
             ,function(err, docs) {
                 if (err){
                   console.log('Error')
                   var response = {
                   "msg" : "Error actualizando iban del usuario"
                   }
                 res.status(400).send(response)
                 }
               else{
                 //console.log(docs)
                 res.status(201).send(iban);
               }
             });
             client.close();
           }
         });
      }
      else{
        var response = {
          "msg" : "Error recuperando iban de mockaroo"
        }
        console.log(response)
        res.status(404).send(response)

      }
    }
  });

}

function getUser(req, res) {
  console.log("GET User");
  console.log(req.params)
  var id = req.params.email;
  var query = 'q={"email":"' + id + '"}';
  console.log(query)
  var httpClient = requestJson.createClient(process.env.BASEMLABURL);
  httpClient.get("user?" + query + "&" + process.env.MLABAPIKEY,
   function(err, resMLab, body){
     if (err){
       var response = {
         "msg" : "Error obteniendo datos de usuario"
       }
       res.status(500).send(response)
     } else {
       if (body.length > 0){
         var response = body;
         res.status(200).send(response)
         console.log(body)
       } else {
         var response = {
           "msg" : "Usuario sin datos"
         }
         res.status(404).send(response)
       }
     }


   }
 );

}

function getAccountById(req, res) {
  console.log("GET getAccountById");
  console.log(req.params)
  var id = req.params.id;
  var query = 'q={"email":"' + id + '"}';
  console.log(query)
  var httpClient = requestJson.createClient(process.env.BASEMLABURL);
  httpClient.get("user?" + query + "&" + process.env.MLABAPIKEY,
   function(err, resMLab, body){
     if (err){
       var response = {
         "msg" : "Error obteniendo cuentas"
       }
       res.status(500)
     } else {
       if (body.length > 0){
         var response = body;
         res.status(200).send(response)
       } else {
         var response = {
           "msg" : "No existen cuentas para este usuario"
         }
         res.status(404).send(response)
       }
     }


   }
 );

}















module.exports.getUser = getUser;
module.exports.createUser = createUser;
module.exports.updateUser = updateUser;
module.exports.getAccountById = getAccountById;
module.exports.createAccount = createAccount;
