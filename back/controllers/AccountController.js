//const io = require('../io');
const dotenv = require('dotenv').config();
const MongoClient = require('mongodb').MongoClient;
//const assert = require('assert');

//const url = "mongodb://admin:gT3xZHn5blKj@ds163730.mlab.com:63730/apitechu"
//const process.env.DBNAME = 'apitechu'
const config  = require('../config');
const requestJson = require('request-json')
//const process.env.BASEMLABURL = "https://api.mlab.com/api/1/databases/apitechu/collections/";
//const process.env.MLABAPIKEY = "apiKey=G16LdGcBQ88ZyjId7l-Kgsa3ENxsOJIG"
const crypt = require('../crypt.js')







function getEvents(req, res) {
  console.log("GET events");

  console.log(req.params.iban)
  iban = req.params.iban
 //  var id = req.params.id;
  var query = 'q={"iban":"' + iban + '"}';
  console.log(query)
  var httpClient = requestJson.createClient(process.env.BASEMLABURL);
  httpClient.get("transactions?" + query + "&" + process.env.MLABAPIKEY,
   function(err, resMLab, body){
     if (err){
       var response = {
         "msg" : "Error obteniendo eventos"
       }
       res.status(500).send(response)
     } else {
       // if (body.length > 0){
         var response = body;
         console.log(body)
         res.status(202).send(body)
       // } else {
       //   var response = {"msg" : "Cuenta sin eventos"}
       //   res.status(404).send(response)
       // }
     }

     //res.send(response);

   }
 );

}


function getBalance(req, res) {
  console.log("GET balance");

  iban = req.params.iban
  MongoClient.connect(process.env.URLMONGO, function(err, client) {
    if (err)
    {
      var response = {
        "msg" : "Error conectando base de datos"
      }
      res.status(500).send(response)

    }
    else
    {
      console.log("Connected successfully to server");
      const db = client.db(process.env.DBNAME);
      const collection = db.collection('transactions')

      collection.aggregate([
          { $match: { "iban": iban }},
          {
              $group:
              {
                  _id: "$iban",
                  balance: {$sum:"$tran"}
              }
          }
        ]).toArray(function(err, docs) {
          if (err){
            var response = {
            "msg" : "Error calculando balance"
            }
          res.status(500).send(response)
          }
        else{
          console.log("balance calculado")
          console.log(docs)
          res.status(201).send(docs[0]);
        }
      });

      client.close();
    }

  });

}



function createEvent(req,res){

  console.log("Post createEvent: ",req.params)
  console.log("Datos: ",req.body)
  datos = req.body
  datos.tran = parseFloat(datos.tran)
  console.log(datos.tran)


  var httpClient = requestJson.createClient(process.env.BASEMLABURL);


  httpClient.post("transactions?"  + process.env.MLABAPIKEY, datos,
   function(err, resMLab, body){

     if (err){
       console.log('ERROR dando de alta la trnsacción')
       console.log(err)
       var response = {
         "msg" : "Error dando de alta la transaccion"
       }
       res.status(500).send(response)
     }
     else{
       console.log("transacción creada con exito")
       console.log("Salida del body: ",body)
       //res.send({"msg": "Usuario creado con éxito"})
       res.status(201).send(body);
     } // Fin Else
   }
  )
}


module.exports.getEvents = getEvents;
module.exports.createEvent = createEvent;
module.exports.getBalance = getBalance;
